from hello.serializers import HelloSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth.models import User
from rest_framework.renderers import JSONRenderer


class HelloView(APIView):
    """
    A view that returns the count of active users in JSON.
    """
    renderer_classes = (JSONRenderer, )

    def get(self, request, format=None):
        serializer = HelloSerializer(data={"name": "qwerqewrasdasdsadqweqwe"})
        serializer.is_valid()
        return Response(serializer.data)

    # def post(self, request):
    #     serializer = HelloSerializer(data={})
    #     serializer.is_valid()
    #     return Somethin

    # def put(self, request):
    #     serializer = HelloSerializer(data={})
    #     serializer.is_valid()
    #     return Somethin

    # def patch(self, request):
    #     serializer = HelloSerializer(data={})
    #     serializer.is_valid()
    #     return Somethin

    # def delete(self, request):
    #     serializer = HelloSerializer(data={})
    #     serializer.is_valid()
    #     return Somethin

