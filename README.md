# Hello Django

## Used commands

```bash
mkdri hello-django
python3 -m venv venv  # creates virtualenv
. venv/bin/acivate   # activates virutalenv

pip install -r requirements.txt  # install dependencies


django-admin startproject lab  # create new project
python mange.py startapp hello  # create new app
```

## Django Rest Framework

serializer + viewset +

## Homework

Make the hello endpoint return something like this:

```json
{
    "name": "Jon",
    "greeting": "Hello",
    "timestamp" "<time that the request was done>"
}
```

Think about serializers!
